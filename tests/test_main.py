import pytest
import os
import sys
import shutil
import pandas as pd
from typing import Generator
import inspect

from sklearn.ensemble import RandomForestRegressor

# Import boston_modeler globally
src_path = os.path.abspath('src')
if src_path not in sys.path:
    sys.path.append(src_path)
# print("Current sys.path:")
# for path in sys.path:
#     print(path)

# from src.modeler.boston_modeler import BostonModeler
from modeler import boston_modeler


@pytest.fixture(scope='session')
def get_data_folder_name() -> str:
    """
    Returns the name of the standard name of the data folder.

    Returns:
        str: The name of the data folder.
    """
    folder_name = "data"
    return folder_name

@pytest.fixture(scope='session')
def get_baddata_folder_name() -> str:
    """
    Returns a non-standard name of the data folder (anything but 'data').

    Returns:
        str: A non-standard name for the data folder.
    """
    folder_name = "baddata"
    return folder_name

@pytest.fixture(scope='session')
def get_testing_data_path() -> str:
    """
    Returns the path of the persistent testing_data folder.

    Returns:
        str: The absolute path of the persistent testing_data folder.
    """
    folder_name = "testing_data"
    return os.path.abspath(os.path.join("tests", folder_name))

@pytest.fixture(scope='session')
def get_data_path() -> str:
    """
    Returns the path of the temporary data folder.

    Returns:
        str: The absolute path of the temporary data folder.
    """
    folder_name = "data"
    return os.path.abspath(os.path.join("tests", folder_name))

@pytest.fixture(scope='session')
def get_baddata_path() -> str:
    """
    Returns the path of the temporary data folder that has a non-standard name (standard is 'data').

    Returns:
        str: The absolute path of the temporary data folder with a non-standard name.
    """
    folder_name = "baddata"
    return os.path.abspath(os.path.join("tests", folder_name))

@pytest.fixture(scope='session')
def get_pst_dataframe_file_name() -> str:
    """
    Returns the path of the DataFrame .csv file in the persistent testing_data folder.

    Returns:
        str: The file name of the DataFrame .csv file in the persistent testing_data folder.
    """
    file_name = "HousingDataKnownPreds.csv"
    return file_name

@pytest.fixture(scope='session')
def get_tmp_dataframe_file_name() -> str:
    """
    Returns the path of the DataFrame .csv file in the temporary data folder.

    Returns:
        str: The file name of the DataFrame .csv file in the temporary data folder.
    """
    file_name = "HousingData.csv"
    return file_name

@pytest.fixture(scope='session')
def get_pst_model_file_name() -> str:
    """
    Returns the path of the Model .pkl file in the persistent testing_data folder.

    Returns:
        str: The file name of the Model .pkl file in the persistent testing_data folder.
    """
    file_name = "boston_housing_model__rfr.pkl"
    return file_name

@pytest.fixture(autouse=True, scope="session")
def rename_root_data_folder(get_data_folder_name: str, get_baddata_folder_name: str) -> Generator[bool, None, None]:
    """
    Temporarily renames the root data folder for testing purposes.

    Args:
        get_data_folder_name (str): The standard name of the data folder.
        get_baddata_folder_name (str): The non-standard name of the data folder.

    Yields:
        bool: True to indicate the fixture is in effect.
    """
    os.rename(get_data_folder_name, get_baddata_folder_name)
    yield True
    os.rename(get_baddata_folder_name, get_data_folder_name)

@pytest.fixture(scope="function")
def tmp_data(get_data_path: str) -> Generator[str, None, None]:
    """
    Creates a temporary data folder for testing.

    Args:
        get_data_path (str): The path to the temporary data folder.

    Yields:
        str: The path to the temporary data folder.
    """
    data_path = get_data_path
    os.makedirs(data_path, exist_ok=True)
    yield data_path
    os.rmdir(data_path)

@pytest.fixture(scope="function")
def tmp_baddata(get_baddata_path: str) -> Generator[str, None, None]:
    """
    Creates a temporary data folder for testing, which has a non-standard name.

    Args:
        get_baddata_path (str): The path to the temporary data folder with a non-standard name.

    Yields:
        str: The path to the temporary data folder with a non-standard name.
    """
    data_path = get_baddata_path
    os.makedirs(data_path, exist_ok=True)
    yield data_path
    os.rmdir(data_path)

@pytest.fixture(scope="function")
def tmp_data_w_df(tmp_data: str, get_testing_data_path: str, get_pst_dataframe_file_name: str, get_tmp_dataframe_file_name: str) -> Generator[str, None, None]:
    """
    Creates a temporary data folder for testing and populates it with a DataFrame .csv file.

    Args:
        tmp_data (str): The path to the temporary data folder.
        get_testing_data_path (str): The path to the persistent testing data folder.
        get_pst_dataframe_file_name (str): The name of the persistent DataFrame file.
        get_tmp_dataframe_file_name (str): The name of the temporary DataFrame file.

    Yields:
        str: The path to the temporary DataFrame file.
    """
    tmp_data_path = tmp_data
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_dataframe_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_tmp_dataframe_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)

@pytest.fixture(scope="function")
def tmp_data_w_model(tmp_data: str, get_testing_data_path: str, get_pst_model_file_name: str) -> Generator[str, None, None]:
    """
    Creates a temporary data folder for testing and populates it with a Model .pkl file.

    Args:
        tmp_data (str): The path to the temporary data folder.
        get_testing_data_path (str): The path to the persistent testing data folder.
        get_pst_model_file_name (str): The name of the persistent Model file.

    Yields:
        str: The path to the temporary Model file.
    """
    tmp_data_path = tmp_data
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_model_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_pst_model_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)

@pytest.fixture(scope="function")
def tmp_baddata_w_df(tmp_baddata: str, get_testing_data_path: str, get_pst_dataframe_file_name: str, get_tmp_dataframe_file_name: str) -> Generator[str, None, None]:
    """
    Creates a temporary data folder for testing, with a non-standard name, and populates it with a DataFrame .csv file.

    Args:
        tmp_baddata (str): The path to the temporary data folder with a non-standard name.
        get_testing_data_path (str): The path to the persistent testing data folder.
        get_pst_dataframe_file_name (str): The name of the persistent DataFrame file.
        get_tmp_dataframe_file_name (str): The name of the temporary DataFrame file.

    Yields:
        str: The path to the temporary DataFrame file.
    """
    tmp_data_path = tmp_baddata
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_dataframe_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_tmp_dataframe_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)

@pytest.fixture(scope="function")
def tmp_baddata_w_model(tmp_baddata: str, get_testing_data_path: str, get_pst_model_file_name: str) -> Generator[str, None, None]:
    """
    Creates a temporary data folder for testing, with a non-standard name, and populates it with a Model .pkl file.

    Args:
        tmp_baddata (str): The path to the temporary data folder with a non-standard name.
        get_testing_data_path (str): The path to the persistent testing data folder.
        get_pst_model_file_name (str): The name of the persistent Model file.

    Yields:
        str: The path to the temporary Model file.
    """
    tmp_data_path = tmp_baddata
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_model_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_pst_model_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)



@pytest.fixture(scope="function")
def change_test_dir(request) -> Generator[None, None, None]:
    """
    Temporarily changes the current working directory to the location of the
    current test file and reverts back after the test. This fixture has a 
    function scope.
    
    Args:
        request: The pytest request object that provides information about the
                 test function being executed.

    Yields:
        None: The context for the test to run with the changed directory.
    """
    test_dir = os.path.dirname(request.fspath)
    original_dir = os.getcwd()
    os.chdir(test_dir)
    yield
    os.chdir(original_dir)


# def test_root_path():
#     """Test that the path is correctly set."""
#    # Get the current working directory
#     cwd = os.getcwd()
#    # Check if 'src' is a directory within the current working directory
#     src_path = os.path.join(cwd, 'src')
#     test_path = os.path.join(cwd, 'tests')
#     assert os.path.isdir(src_path), f"Expected 'src' directory in the current working directory, found cwd: {cwd}"
#     assert os.path.isdir(test_path), f"Expected 'tests' directory in the current working directory, found cwd: {cwd}"

def test_data_path(tmp_data):
    """Tests if the temporary data folder has been created"""
    # print("tmp_data")
    assert os.path.isdir(tmp_data)

def test_tmp_files_presence(get_data_path,get_baddata_path,tmp_data_w_df,tmp_data_w_model, tmp_baddata_w_df, tmp_baddata_w_model):
    """Tests if the temporary files in the temporary data folder have been created"""
    tmp_df_path = tmp_data_w_df
    tmp_model_path = tmp_data_w_model
    tmp_baddf_path = tmp_baddata_w_df
    tmp_badmodel_path = tmp_baddata_w_model    

    assert os.path.isfile(tmp_df_path)
    assert os.path.isfile(tmp_model_path)
    assert os.path.isfile(tmp_baddf_path)
    assert os.path.isfile(tmp_badmodel_path)

def test_renaming_of_data_folder(rename_root_data_folder):
    """
    Test if the root data folder has been renamed temporarily to avoid 
    conflicts.
    """
    assert rename_root_data_folder==True

def test_class_instantiation(tmp_data,change_test_dir):
    print(f"\n{inspect.currentframe().f_code.co_name}:")
    # print("test", os.getcwd())
    tmp = tmp_data
    # print(tmp)
    BM_object = boston_modeler.BostonModeler("rfr")
    assert isinstance(BM_object, boston_modeler.BostonModeler)

def test_ci_with_df(tmp_data_w_df,change_test_dir):
    print(f"\n{inspect.currentframe().f_code.co_name}:")
    tmp = tmp_data
    BM_object = boston_modeler.BostonModeler("rfr","data", load_data="df")
    df_object = BM_object.return_df()
    assert isinstance(df_object, pd.DataFrame)


def test_ci_with_model(tmp_data_w_model,change_test_dir):
    print(f"\n{inspect.currentframe().f_code.co_name}:")
    tmp = tmp_data_w_model
    print("tmp:", tmp)
    print(os.path.isfile(tmp))
    print(os.getcwd())
    BM_object = boston_modeler.BostonModeler("rfr","data", load_data="model")
    model_object = BM_object.return_model()
    print(type(model_object))
    assert isinstance(model_object, RandomForestRegressor)
