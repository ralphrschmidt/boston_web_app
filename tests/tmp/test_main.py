import pytest
import os
import sys
import shutil
import pandas as pd

# Import boston_modeler globally
src_path = os.path.abspath('src')
if src_path not in sys.path:
    sys.path.append(src_path)
# print("Current sys.path:")
# for path in sys.path:
#     print(path)

# from src.modeler.boston_modeler import BostonModeler
from modeler import boston_modeler


@pytest.fixture(scope='session')
def get_data_folder_name():
    """Returns the name of the standard name of the data folder"""
    folder_name = "data"
    return folder_name

@pytest.fixture(scope='session')
def get_baddata_folder_name():
    """Returns a non-standard name of the data folder (anything but 'data')"""
    folder_name = "baddata"
    return folder_name


@pytest.fixture(scope='session')
def get_testing_data_path():
    """Returns the path of the persistent testing_data folder"""
    folder_name = "testing_data"
    return os.path.abspath(os.path.join("tests", folder_name))

@pytest.fixture(scope='session')
def get_data_path():
    """Returns the path of the temporary data folder"""
    folder_name = "data"
    return os.path.abspath(os.path.join("tests", folder_name))

@pytest.fixture(scope='session')
def get_baddata_path():
    """Returns the path of the temporary data folder, that has a non-standard name (standard is 'data')"""
    folder_name = "baddata"
    return os.path.abspath(os.path.join("tests", folder_name))


@pytest.fixture(scope='session')
def get_pst_dataframe_file_name():
    """Returns the path of the DataFrame .csv file in the persistent testing_data folder"""
    file_name = "HousingDataKnownPreds.csv"
    return file_name

@pytest.fixture(scope='session')
def get_tmp_dataframe_file_name():
    """Returns the path of the DataFrame .csv file in the temporary data folder"""
    file_name = "HousingData.csv.csv"
    return file_name

@pytest.fixture(scope='session')
def get_pst_model_file_name():
    """Returns the path of the Model .pkl file in the persistent testing_data folder"""
    file_name = "boston_housing_model__rfr.pkl"
    return file_name


@pytest.fixture(autouse=True, scope="session")
def rename_root_data_folder(get_data_folder_name, get_baddata_folder_name):
    """
    Temporarily renames root data folder, for testing purposed (the Modeler class)
    contains a function to go up the project directory to see if it finds the data
    folder somewhere).
    """
    os.rename(get_data_folder_name, get_baddata_folder_name)
    yield True
    os.rename(get_baddata_folder_name, get_data_folder_name)

@pytest.fixture(scope="function")
def tmp_data(get_data_path, ):
    """Creates temporary data folder for testing"""
    data_path = get_data_path
    os.makedirs(data_path,exist_ok =True)
    yield data_path
    os.rmdir(data_path)

@pytest.fixture(scope="function")
def tmp_baddata(get_baddata_path):
    """Creates temporary data folder for testing, which has a non-standard name"""
    data_path = get_baddata_path
    os.makedirs(data_path,exist_ok =True)
    yield data_path
    os.rmdir(data_path)


@pytest.fixture(scope="function")
def tmp_data_w_df(tmp_data, get_testing_data_path, get_pst_dataframe_file_name, get_tmp_dataframe_file_name):
    """Creates temporary data folder for testing, and populates it with a DataFrame .csv file"""
    tmp_data_path = tmp_data
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_dataframe_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_tmp_dataframe_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)

@pytest.fixture(scope="function")
def tmp_data_w_model(tmp_data, get_testing_data_path, get_pst_model_file_name):
    """Creates temporary data folder for testing, and populates it with a Model .pkl file"""
    tmp_data_path = tmp_data
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_model_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_pst_model_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)


@pytest.fixture(scope="function")
def tmp_baddata_w_df(tmp_baddata, get_testing_data_path, get_pst_dataframe_file_name, get_tmp_dataframe_file_name):
    """Creates temporary data folder for testing, with non-standard name, and populates it with a DataFrame .csv file"""
    tmp_data_path = tmp_baddata
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_dataframe_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_tmp_dataframe_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)

@pytest.fixture(scope="function")
def tmp_baddata_w_model(tmp_baddata, get_testing_data_path, get_pst_model_file_name):
    """Creates temporary data folder for testing, with non-standard name, and populates it with a Model .pkl file"""
    tmp_data_path = tmp_baddata
    file_pst_path = os.path.abspath(os.path.join(get_testing_data_path, get_pst_model_file_name))
    file_tmp_path = os.path.abspath(os.path.join(tmp_data_path, get_pst_model_file_name))
    shutil.copyfile(file_pst_path, file_tmp_path)
    yield file_tmp_path
    os.remove(file_tmp_path)




# def test_root_path():
#     """Test that the path is correctly set."""
#    # Get the current working directory
#     cwd = os.getcwd()
#    # Check if 'src' is a directory within the current working directory
#     src_path = os.path.join(cwd, 'src')
#     test_path = os.path.join(cwd, 'tests')
#     assert os.path.isdir(src_path), f"Expected 'src' directory in the current working directory, found cwd: {cwd}"
#     assert os.path.isdir(test_path), f"Expected 'tests' directory in the current working directory, found cwd: {cwd}"

def test_data_path(tmp_data):
    # print("tmp_data")
    assert os.path.isdir(tmp_data)

def test_tmp_files_presence(get_data_path,get_baddata_path,tmp_data_w_df,tmp_data_w_model, tmp_baddata_w_df, tmp_baddata_w_model):
    tmp_df_path = tmp_data_w_df
    tmp_model_path = tmp_data_w_model
    tmp_baddf_path = tmp_baddata_w_df
    tmp_badmodel_path = tmp_baddata_w_model    

    assert os.path.isfile(tmp_df_path)
    assert os.path.isfile(tmp_model_path)
    assert os.path.isfile(tmp_baddf_path)
    assert os.path.isfile(tmp_badmodel_path)

def test_renaming_of_data_folder(rename_root_data_folder):
    assert rename_root_data_folder==True

def test_class_instantiation(tmp_data):
    tmp = tmp_data
    print(tmp)
    BM_object = boston_modeler.BostonModeler("rfr")
    assert isinstance(BM_object, boston_modeler.BostonModeler)


