import os
import sys



def set_working_directory():
    """
    Change the working directory to the root of the tests directory at the start of the tests.
    """
    print(f"Current working directory: {os.getcwd()}")
    file_path = os.path.dirname(__file__)
    print(f"file_path: {file_path}")
    root_path = os.path.dirname(file_path)
    print(f"root_path: {root_path}")
    os.chdir(os.path.abspath(root_path))
    print(f"Current working directory after change: {os.getcwd()}")

def import_bosot_modeler():
    sys.path.append(os.path.abspath('src'))
    print("Current Python path:")
    for path in sys.path:
        print(path)

    import boston_modeler

if __name__ == "__main__":
    set_working_directory()
    import_bosot_modeler()