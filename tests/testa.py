import pytest
import os
import sys
import shutil
import pandas as pd

# Import boston_modeler globally
src_path = os.path.abspath('src')
if src_path not in sys.path:
    sys.path.append(src_path)
# print("Current sys.path:")
# for path in sys.path:
#     print(path)

# from src.modeler.boston_modeler import BostonModeler
from modeler import boston_modeler



# def set_working_directory():
#     """
#     Change the working directory to the root of the tests directory at the start of the tests.
#     """
#     print(f"Current working directory: {os.getcwd()}")
#     file_path = os.path.dirname(__file__)
#     print(f"file_path: {file_path}")
#     root_path = os.path.dirname(file_path)
#     print(f"root_path: {root_path}")
#     os.chdir(os.path.abspath(root_path))
#     print(f"Current working directory after change: {os.getcwd()}")


if __name__ == "__main__":
    # set_working_directory()
    BM_object = boston_modeler.BostonModeler("rfr")