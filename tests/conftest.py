# conftest.py
import pytest
import os
import sys

import pandas as pd

@pytest.fixture(autouse=True, scope='session')
def set_working_directory():
    """
    Change the working directory to the root of the tests directory at the start of the tests.
    """
    # print(f"Current working directory: {os.getcwd()}")
    file_path = os.path.dirname(__file__)
    # print(f"file_path: {file_path}")
    root_path = os.path.dirname(file_path)
    # print(f"root_path: {root_path}")
    os.chdir(os.path.abspath(root_path))
    # print(f"Current working directory after change: {os.getcwd()}")
    
# @pytest.fixture(autouse=True, scope='session')
# def set_module_dir():
#     src_path = os.path.abspath('src')
#     if src_path not in sys.path:
#         sys.path.append(src_path)
#     print("Current sys.path:", sys.path)  # Debug: Print to confirm path



# @pytest.fixture(autouse=False, scope='session')
# def test_cases():
#     """
#     Loading DataFrame containing test cases
#     """
#     df = pd.read_csv(os.path.join("tests","data", "HousingDataKnownPreds.csv"))
#     df = df[df["TEST_CASE"] == 1]
#     return df




# @pytest.fixture(autouse=False, scope='session')
# def boston_modeler(tmp_path):
#     """
#     Fixture to create a BostonModeler instance with a temporary data path.
#     """
#     # Add the directory containing the modeler module to the Python path
#     sys.path.append(os.path.join("..", "src" ))  # Replace '/path/to/modeler_dir' with the actual path

#     # Import the BostonModeler class from the modeler module
#     from modeler import BostonModeler

#     # Create a temporary data directory
#     data_path = tmp_path / "data"
#     data_path.mkdir()

#     # Assuming you have a DataFrame 'df' available here
#     df.to_csv(data_path / "HousingData.csv", index=False)

#     # Create and return a BostonModeler instance
#     return BostonModeler(model_type='RFR', data_path=str(data_path))
    