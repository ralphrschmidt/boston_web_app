import numpy as np
import pandas as pd
import pytest
from modeler import BostonModeler

@pytest.fixture
def boston_modeler(tmp_path):
    """Fixture to create a BostonModeler instance with a temporary data path."""
    data_path = tmp_path / "data"
    data_path.mkdir()
    # Assuming a DataFrame setup needed for testing
    df = pd.DataFrame({
        'CRIM': [0.00632],
        'ZN': [18.0],
        'INDUS': [2.31],
        'CHAS': [0],
        'NOX': [0.538],
        'RM': [6.575],
        'AGE': [65.2],
        'DIS': [4.0900],
        'RAD': [1],
        'TAX': [296],
        'PTRATIO': [15.3],
        'B': [396.9],
        'LSTAT': [4.98]
    })
    
    df.to_csv(data_path / "HousingData.csv", index=False)
    return BostonModeler(model_type='RFR', data_path=str(data_path))

def test_train(boston_modeler, mocker):
    """Test the training method."""
    mocked_fit = mocker.patch.object(boston_modeler, 'model', autospec=True)
    boston_modeler.train()
    assert mocked_fit.fit.called

def test_predict(boston_modeler, mocker):
    """Test the predict function."""
    test_df = pd.DataFrame({  # Example DataFrame matching the expected input
        'CRIM': [0.1], 'ZN': [0], 'INDUS': [10], 'CHAS': [0], 'NOX': [0.5],
        'RM': [5], 'AGE': [70], 'DIS': [4], 'RAD': [1], 'TAX': [300],
        'PTRATIO': [20], 'B': [350], 'LSTAT': [12]
    })
    expected = np.array([22])
    mocker.patch.object(boston_modeler, 'model', autospec=True, predict=lambda x: expected)
    prediction = boston_modeler.predict(test_df)
    np.testing.assert_array_equal(prediction, expected)

def test_save_and_load(boston_modeler, mocker, tmp_path):
    """Test saving and loading the model."""
    model_file = tmp_path / "model.pkl"
    mocker.patch('pickle.dump')
    mocker.patch('builtins.open', mocker.mock_open())
    boston_modeler.save(model_file)
    mocker.patch('pickle.load', return_value=mocker.MagicMock())
    boston_modeler.load(model_file)
    assert open.called
    assert pickle.dump.called
    assert pickle.load.called

def test_save_check(boston_modeler, tmp_path):
    """Test the save_check function."""
    model_file = tmp_path / "model.pkl"
    assert not boston_modeler.save_check(model_file)  # Should return False initially
    model_file.touch()  # Create the file
    assert boston_modeler.save_check(model_file)  # Now should return True

# More tests can be added as needed