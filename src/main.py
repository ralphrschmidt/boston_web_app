import os
import subprocess
import sys
import signal

# Relative path to the PID file from the src directory
PID_PATH = "../streamlit.pid"
PWD = os.path.realpath(__file__) # Current file path

def start_app():
    if os.path.exists(PID_PATH):
        print("Streamlit app is already running.")
        return

    # Start the Streamlit app as a subprocess
    streamlit_process = subprocess.Popen(["streamlit", "run", "app.py"])
    with open(PID_PATH, "w") as pid_file:
        pid_file.write(str(streamlit_process.pid))

    print("Streamlit app started.")

def stop_app():
    if not os.path.exists(PID_PATH):
        print("Streamlit app is not running.")
        return

    with open(PID_PATH, "r") as pid_file:
        pid = int(pid_file.read())

    try:
        os.kill(pid, signal.SIGTERM)  # Send termination signal to the process
        os.remove(PID_PATH)  # Remove the PID file
        print("Streamlit app stopped.")
    except ProcessLookupError:
        print("No such process. Cleaning up PID file.")
        os.remove(PID_PATH)

if __name__ == "__main__":
    os.chdir(os.path.dirname(PWD))

    if len(sys.argv) != 2:
        print("Usage: python main.py <start|stop>")
    else:
        command = sys.argv[1]
        if command == "start":
            start_app()
        elif command == "stop":
            stop_app()
        else:
            print("Invalid command. Use 'start' or 'stop'.")
