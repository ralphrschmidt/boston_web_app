# Importing modules
import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# Importing custom module
PWD = os.path.abspath(r"C:\msys64\home\ral31126\Projects\240502__simple_web_app\src")
os.chdir(PWD)
from modeler import BostonModeler

# Global variables
FEATURES = [
    "CRIM",
    "ZN",
    "INDUS",
    "CHAS",
    "NOX",
    "RM",
    "AGE",
    "DIS",
    "RAD",
    "TAX",
    "PTRATIO",
    "B",
    "LSTAT"]


if __name__ == "__main__":

    # Loading Model
    bm = BostonModeler("rfr", os.path.join(PWD, "..","data"))
    bm.load("boston_housing_model")

    # Saving Boston housing data in variable
    df = bm.return_df()

    # Generating list of test cases (denoted by a 1)
    test_case = np.zeros(df.shape[0],dtype=int)

    test_case_indices = [0,149,233,370,387,503]

    test_case[test_case_indices] = 1
    
    # Generating list of predictions
    Y_pred = bm.predict(df[FEATURES])

    # Adding test cases and predictions to data frame
    df["TEST_CASE"] = test_case
    df["MEDV_PRED"] = Y_pred

    # Saving data frame in test/data folder
    df.to_csv(os.path.join(PWD, "..", "tests","data","HousingDataKnownPreds.csv"))