#%% Imports
import os
import pickle
from typing import Tuple, Any, List

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import shap
import streamlit as st
from sklearn.ensemble import RandomForestRegressor

from modeler import BostonModeler


#%% Functions

# Cache SHAP explainer and values
@st.cache_data
def get_shap_values(_model: RandomForestRegressor, X: pd.DataFrame) -> np.ndarray:
    """
    Compute SHAP values using a RandomForestRegressor model and feature data.

    Args:
    _model (RandomForestRegressor): The trained model.
    X (pd.DataFrame): Feature data used for computing SHAP values.

    Returns:
    np.ndarray: Computed SHAP values.
    """
    explainer = shap.TreeExplainer(_model)
    shap_values = explainer.shap_values(X)
    return shap_values

# Sidebar for input parameters
def user_input_features(df: pd.DataFrame) -> pd.DataFrame:
    """
    Generate a DataFrame of user inputs from Streamlit sidebar widgets based on DataFrame columns.
    Also, the Streamlit sidebar widgets are initialized and added to the app.

    Args:
    df (pd.DataFrame): DataFrame to use for determining the range and default values of inputs.

    Returns:
    pd.DataFrame: DataFrame containing user-specified input values.
    """
    CRIM = st.sidebar.slider('CRIM', X.CRIM.min(), X.CRIM.max(), X.CRIM.mean())
    ZN = st.sidebar.slider('ZN', X.ZN.min(), X.ZN.max(), X.ZN.mean())
    INDUS = st.sidebar.slider('INDUS', X.INDUS.min(), X.INDUS.max(), X.INDUS.mean())
    CHAS = st.sidebar.slider('CHAS', X.CHAS.min(), X.CHAS.max(), X.CHAS.mean())
    NOX = st.sidebar.slider('NOX', X.NOX.min(), X.NOX.max(), X.NOX.mean())
    RM = st.sidebar.slider('RM', X.RM.min(), X.RM.max(), X.RM.mean())
    AGE = st.sidebar.slider('AGE', X.AGE.min(), X.AGE.max(), X.AGE.mean())
    DIS = st.sidebar.slider('DIS', X.DIS.min(), X.DIS.max(), X.DIS.mean())
    
    RAD = st.sidebar.slider('RAD', min_value=X.RAD.min(), max_value=X.RAD.max(), value=int(X.RAD.mean()),step=1)
    TAX = st.sidebar.slider('TAX', X.TAX.min(), X.TAX.max(), int(X.TAX.mean()), step=int((X.TAX.max()-X.TAX.min())/10))

    PTRATIO = st.sidebar.slider('PTRATIO', X.PTRATIO.min(), X.PTRATIO.max(), X.PTRATIO.mean())
    B = st.sidebar.slider('B', X.B.min(), X.B.max(), X.B.mean())
    LSTAT = st.sidebar.slider('LSTAT', X.LSTAT.min(), X.LSTAT.max(), X.LSTAT.mean())
    data = {'CRIM': CRIM,
            'ZN': ZN,
            'INDUS': INDUS,
            'CHAS': CHAS,
            'NOX': NOX,
            'RM': RM,
            'AGE': AGE,
            'DIS': DIS,
            'RAD': RAD,
            'TAX': TAX,
            'PTRATIO': PTRATIO,
            'B': B,
            'LSTAT': LSTAT}
    features = pd.DataFrame(data, index=[0])
    return features

# Plot 1  - Bee-Swarm Plot
@st.cache_data
def create_shap_summary_plot(shap_values: np.ndarray, features: pd.DataFrame) -> plt.Figure:
    """
    Create a SHAP summary plot (bee-swarm) using SHAP values and feature data.

    Args:
    shap_values (Any): SHAP values computed from the model.
    features (pd.DataFrame): Feature data used in the SHAP value computation.

    Returns:
    plt.Figure: The matplotlib figure object containing the SHAP summary plot.
    """
    fig, ax = plt.subplots()
    ax.set_title('Feature importance based on SHAP values')
    shap.summary_plot(shap_values, features, show=False)
    fig = plt.gcf()
    return fig

# Plot 2  - Bar-Chart
@st.cache_data
def create_shap_summary_bar_plot(shap_values: Any, features: pd.DataFrame) -> plt.Figure:
    """
    Create a SHAP summary bar plot using SHAP values and feature data.

    Args:
    shap_values (Any): SHAP values computed from the model.
    features (pd.DataFrame): Feature data used in the SHAP value computation.

    Returns:
    plt.Figure: The matplotlib figure object containing the SHAP summary bar plot.
    """
    fig, ax = plt.subplots()
    ax.set_title('Feature importance based on SHAP values (Bar)')
    shap.summary_plot(shap_values, features, show=False, plot_type="bar")
    return fig

    
#%% Global variables
FEATURES = [
    "CRIM",
    "ZN",
    "INDUS",
    "CHAS",
    "NOX",
    "RM",
    "AGE",
    "DIS",
    "RAD",
    "TAX",
    "PTRATIO",
    "B",
    "LSTAT"]

TARGET = ["MEDV"]

PWD = os.path.join(os.path.expanduser("~"), "Projects/240502__simple_web_app/src")
PDD = "../data" 
MODEL_FILE = "boston_housing_model"

H1 = "Boston House Price Prediction App"
H2_INPUT_PARAM = "Specified Inut Parameters"
H2_PREDICTION_OUT = "Prediction of MEDV"
H2_FEATURE_IMPORTANCE = "Feature Importance"

PREDICTION_OUT_PRE_TEXT = "Median value of owner-occupied homes in $1000s: "
APP_DESCRIPTION = """
# Boston House Price Prediction App

This app predicts the **Boston House Price**!
"""

#%% Main

if __name__ == "__main__":
    # Setting working directory
    os.chdir(PWD)

    # Main Panel
    st.write(APP_DESCRIPTION)
    st.write('---')

    # Loads the Boston House Price Dataset
    bm = BostonModeler("rfr", os.path.join(PWD, "../data"))
    df = bm.return_df()
    X, Y = bm.return_X_Y()


    # Load Regression Model
    # Checks if saved model is there, if not a new model is trained
    if bm.save_check(MODEL_FILE):
        bm.load(MODEL_FILE)
    else:
        bm.train()


    # Sidebar
    # Header of Specify Input Parameters
    st.sidebar.header(H2_INPUT_PARAM)

    # Obtaining use inputed features via sidebar
    df_slt = user_input_features(df)

    # Print specified input parameters
    st.header(H2_INPUT_PARAM )
    st.dataframe(df_slt, hide_index=True)
    st.write('---')

    # Apply Model to Make Prediction
    prediction = bm.predict(df_slt)

    # Main Panel
    st.header(H2_PREDICTION_OUT)

    # st.write(f" Median value of owner-occupied homes in $1000s: **{prediction.round(1)[0]}**")
    st.metric(label=PREDICTION_OUT_PRE_TEXT, value=f"{prediction.round(1)[0]}", delta=None)
    st.write('---')

    # Explaining the model's predictions using SHAP values
    # https://github.com/slundberg/shap
    shap_values = get_shap_values(bm.return_model(), X)


    # Main Panel
    st.header(H2_FEATURE_IMPORTANCE)

    # Plot 1: Standard SHAP Summary Plot
    fig1 = create_shap_summary_plot(shap_values, X)
    st.pyplot(fig1,bbox_inches='tight')
    st.write('---')

    # Plot 2: SHAP Summary Bar Plot
    fig2 = create_shap_summary_bar_plot(shap_values, X)
    st.pyplot(fig2,bbox_inches='tight')
# %%
