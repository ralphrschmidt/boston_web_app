from modeler import boston_modeler

print("BM1:")
bm1 = boston_modeler.BostonModeler("rfr")
print()
print("BM2:")
bm2 = boston_modeler.BostonModeler("rfr", load_data="df")
print()
print("BM3:")
bm3 = boston_modeler.BostonModeler("rfr", load_data="model")
print()
print("BM4:")
bm4 = boston_modeler.BostonModeler("rfr", load_data="df_model")
print()

print("BM5:")
bm1 = boston_modeler.BostonModeler("rfr", data_path="this_is_not_a_path")
print()
print("BM6:")
bm2 = boston_modeler.BostonModeler("rfr", data_path="this_is_not_a_path", load_data="df")
print()
print("BM7:")
bm3 = boston_modeler.BostonModeler("rfr", data_path="this_is_not_a_path", load_data="model")
print()
print("BM8:")
bm4 = boston_modeler.BostonModeler("rfr", data_path="this_is_not_a_path", load_data="df_model")



from sklearn.ensemble import RandomForestRegressor

