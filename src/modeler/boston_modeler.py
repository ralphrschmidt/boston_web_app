import pickle
import os
import inspect
from typing import Tuple, Any, Optional, List
from warnings import warn
import warnings

import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
import streamlit as st

from .modeler import Modeler
 
warnings.simplefilter("always")

CURRENT_FILE_NAME = "boston_modeler.py"
MAX_FILE_LOCATION_DEPTH = 5
INVALID_PATH_PARTS = ["site-packages"] 


class BostonModeler(Modeler):
    # def __init__(self, model_type: str, data_path: Optional[str] = None, load_model: Optional[bool] = True, load_df: Optional[bool] = True):
    def __init__(self, model_type: str, data_path: Optional[str] = None, load_data: Optional[str] = "None"):
        """
        Initialize the BostonModeler instance.

        Parameters:
            model_type (str): The type of model to train.
            data_path (Optional[str]): Optional string to adjust the path of
                the data directory which may include the dataframe and saved models.
        """
        # Variables to be initiated
        super().__init__(model_type=model_type, data_path=data_path)
        
        self.df = None
        self.model = None
        self.model_type = model_type
        self.load_data = load_data
        self.df_file_name = "HousingData"
        self.model_file_name = "boston_housing_model__" + self.model_type.lower()
        # Function that initializes self.df and self.model, based on the supplied parameters.
        self._init_data_vars()
        
        self.features = [
            "CRIM",
            "ZN",
            "INDUS",
            "CHAS",
            "NOX",
            "RM",
            "AGE",
            "DIS",
            "RAD",
            "TAX",
            "PTRATIO",
            "B",
            "LSTAT"]
        self.target = ["MEDV"]
        
        if self.df is not None:
            self.X = self.df[self.features]
            self.Y = self.df[self.target].to_numpy().ravel()
        else:
            self.X = None
            self.Y = None

        self.mode = None

    def _init_data_vars(self) -> None:
        # If a data path has been provided
        if (self.data_path):
            print("Data path provided")
            # print()
            # If the data path provided is valid
            if os.path.isdir(self.data_path):
                self._load_all_data(self.data_path)
            # If the data path provided is no valid
            else:
                tmp_data_path = self.data_path
                self.data_path = self._find_data_path()
                # If the data path is found automatically:
                if self.data_path:
                    msg = (
                        f"The data path provided ('{tmp_data_path }') does not exit. "
                        "However, the data path has been determined automatically "
                        f"and is: {self.data_path}"
                    )
                    warn(msg)
                    self._load_all_data(self.data_path)
                # If the data path is not found automatically:
                else:
                    msg = (
                        f"The data path provided ('{tmp_data_path }') does not exit and "
                        "the data path could not be determined automatically. "
                        "Therefore, it will be set to None. Please provide one using "
                        "'set_data_path'. DataFrame and Model will not be loaded."
                    )
                    warn(msg)
                    self.data_path = None
                    self.df = None
                    self.model = None

        # If no data path has been provided
        else:
            print("Data path not provided")
            self.data_path = self._find_data_path()
            if self.data_path:
                msg = (
                    "Data path was not provided, but determined automatically "
                    f"to be '{self.data_path}'"
                )
                warn(msg)
                self._load_all_data(self.data_path)
                # print("if")
            else:
                msg = (
                    "The data path was not provided and could not be determined. "
                    "Therefore, it will be set to None. Please provide one using "
                    "'set_data_path'. DataFrame and Model will not be loaded."
                )
                # print("else")
                warn(msg)
                self.data_path = None
                self.df = None
                self.model = None


    def _load_all_data(self, data_path: str) -> None:
        # Setting flags if files are there
        df_file_path =      os.path.join(data_path, self.df_file_name+".csv" )
        model_file_path =   os.path.join(data_path, self.model_file_name+".pkl")
        print("os.listdir():", os.listdir(data_path))

        df_flag = os.path.isfile(df_file_path)
        print("model_file_path:", model_file_path)
        model_flag = os.path.isfile(model_file_path)
        print("model_flag:", model_flag)

        match str(self.load_data):
            case "df":
                print("'df' was selected as an argument for load_data.")
                if (df_flag):
                    # print("DataFrame is loaded")
                    self.load_df(self.df_file_name)
                    self.model = None
                else:
                    msg = (
                        f"The DataFrame at '{df_file_path}' could not be found. "
                        "No DataFrame will be loaded. Also, load_data will be set "
                        "to 'None'."
                    )
                    warn(msg)
                    self.df = None
                    self.model = None
                    self.load_data = None
                    
            case "model":
                print("'model' was selected as an argument for load_data.")
                if (model_flag):
                    # print("Model is loaded")
                    self.df = None
                    self.load_model(self.model_file_name)
                else:
                    msg = (
                        f"The model at '{model_file_path}' could not be found. "
                        "No Model will be loaded. Also, load_data will be set "
                        "to 'None'."
                    )
                    warn(msg)
                    self.df = None
                    self.model = None
                    self.load_data = None

            case "df_model":
                print("'df_model' was selected as an argument for load_data.")
                # If DataFrame exists but Model does not:
                if (df_flag and not model_flag):
                    # print("DataFrame is loaded")
                    msg = (
                        f"The Model at '{model_file_path}' could not be found. "
                        "Loading the DataFrame only. Also, load_data is set to "
                        "'df'."
                    )
                    warn(msg)
                    self.load_df(self.df_file_name)
                    self.model = None
                    self.load_data = "df"

                # If DataFrame does not exists but the Model does:
                elif (model_flag and not df_flag):
                    # print("Model is loaded")
                    msg = (
                        f"The DataFrame at '{df_file_path}' could not be found. "
                        "Loading the model only. Also, load_data is set to 'model'."
                    )
                    warn(msg)
                    self.df = None
                    self.load_model(self.model_file_name)
                    self.load_data = "model"

                # If DataFrame and the Model exist:
                elif (df_flag and model_flag):
                    # print("DataFrame and Model are loaded")
                    # print("_load_all_data 1: self.data_path",self.data_path)
                    self.load_df(self.df_file_name)
                    # print("_load_all_data 2: self.data_path",self.data_path)
                    self.load_model(self.model_file_name)

                # If DataFrame nor the Model exist:
                else: # (not df_flag and not model_flag)
                    msg = (
                        f"The dataframe does not exist in: '{df_file_path}'. Also, "
                        f" the model does not exist in: '{model_file_path}'. Thus, "
                        "the DataFrame and Model are not loaded. Please, load "
                        "them manually with load_new_df() and/ load_new_model()."
                    )
                    warn(msg)
                self.df = None
                self.model = None
                self.load_data = None

            case "None":
                print("'None' was selected as an argument for load_data.")
                self.df = None
                self.model = None

            case _:
                msg = (
                    f"'{self.load_data}' was selected for the load_data parameter. "
                    "This is not a valid option. Valid options include: df, "
                    "model, df_model and None. No DataFram or model are loaded."
                    "Also, load_data is set to 'None'" 
                )
                warn(msg)
                self.df = None
                self.model = None
                self.load_data = None

    # def _find_potential_caller_files(self):
    #     caller_files = []
    #     for i, entry in enumerate(inspect.stack()):
    #         file_path = os.path.normpath(entry)
    #         invalid_flag = any(invalid_path_part in file_path for invalid_path_part in INVALID_PATH_PARTS)
    #         directory, file_name = os.path.split(file_path)
    #         if ((file_name.lower() != CURRENT_FILE_NAME.lower()) and (not invalid_flag)):
    #             caller_files.append(file_path)
    #     return caller_files

    # def _find_potential_calling_file_locations(self, max_depth: Optional[int] = 5) -> List[str]:
    #     current_dir = None
    #     caller_file = None
    #     for i, entry in enumerate(inspect.stack()):
    #         file_path = os.path.normpath(entry.filename)
    #         directory, file_name = os.path.split(file_path)
    #         if (file_name.lower() != CURRENT_FILE_NAME.lower()):
    #             caller_file = file_path
    #     file_path = os.path.normpath(path)
    #     pass

    def _find_project_root(self, start_path: Optional[str] = None) -> Optional[str]:
        """
        Find the root directory containing the "src" folder.

        Args:
            start_path (Optional[str]): The starting directory to begin the search.
                                        Defaults to the current working directory.

        Returns:
            Optional[str]: The path of the root directory containing the "src" folder,
                        or None if the "src" folder is not found.
        """
        # Setting default parameter
        if start_path == None: start_path = os.getcwd()
       
        start_path: str = start_path
        
        # Iterate over parent directories until the "src" folder is found
        while True:
            if "src" in os.listdir(start_path):
                return start_path  # Found the root directory containing "src"
            # Move to the parent directory
            parent_path: str = os.path.dirname(start_path)
            # Check if we've reached the root directory
            if parent_path == start_path:
                return None  # "src" folder not found
            start_path = parent_path

    def _find_data_path(self, current_directory: Optional[str] = None) -> Optional[str]:
        """
        Find the data directory if it is inside the current directory of the
        caller file or the project root (by trying to find the project root first).

        Args:
            current_directory (Optional[str]): The directory to start searching from.
                                            Defaults to the current working directory.

        Returns:
            Optional[str]: The path of the data directory, or None if the "data" folder
                        is not found.
        """
        # Setting default parameter
        if current_directory == None: current_directory = os.getcwd()

        if "data" in os.listdir(current_directory):
            data_folder = os.path.join(current_directory, "data")
            if os.path.isdir(data_folder):
                return data_folder  # Found the data directory in current_directory
        else:
            # Attempt to find the "data" folder in the project root
            project_root = self._find_project_root(start_path=current_directory)
            if project_root:
                data_folder = os.path.join(project_root, "data")
                if os.path.exists(data_folder) and os.path.isdir(data_folder):
                    return data_folder  # Found the data directory in project root
        
        return None  # "data" folder not found in current directory or project root


    def train(self) -> None:
        """
        Train the model using the dataframe self.df.
        """
        if self.model_type.lower() == "RFR".lower():
            _trainRFR()
        else: raise ValueError(f"Invalid option '{self.model_type}' specified. Valid options are: RFR")
        
        self.model.fit(self.X, self.Y)

    def _trainRFR(self) -> None:
        self.model = RandomForestRegressor()

    def predict(self, df:pd.DataFrame) -> np.ndarray:
        """
        Makes a prediction using the given data from a dataframe 
        (one observation) and returns it.

        Parameters:
            df (pd.DataFrame): One observation of a model compatible dataframe.
        
        Returns:
            np.ndarray: An array containing the prediction.

        """
        return self.model.predict(df)

    def set_data_path(self, new_data_path:str, create_dir: Optional[bool] = True) -> None:
        """
        Set the data directory path, that may include the model picke file and
        the training .csv file.

        Parameter: 
            new_data_path (str): A path compatible string variabe
            create_dir (Optional[bool]): An optional parameter that controls 
                whether a new directory should be created if the given path
                does not exist. By default, a new directory will be made if it
                did not exist.
        """
        # print("set_data_path 1: new_data_path:", new_data_path)
        if not isinstance(new_data_path, str):
            # print("set_data_path 2: new_data_path:", new_data_path)
            raise TypeError("The new data path specified has to be a valid path string.")
        if not os.path.exists(new_data_path):
            # print("set_data_path 3: new_data_path:", new_data_path)
            if create_dir:
                print("New data path did not exist, it will be created now")
                os.makedirs(new_data_path)  # create the directory
                # print("set_data_path 4: new_data_path:", new_data_path)
                self.data_path = new_data_path
            else:
                raise FileNotFoundError(f"The specified path '{new_data_path}' does not exist.")
        else:
            # print("set_data_path 5: new_data_path:", new_data_path)
            self.data_path = new_data_path

    def save_df(self, filename:Optional[str] = None) -> None:
        """
        Save the DataFrame to a file.

        Parameters:
            filename (str, optional): The name of the file to save the DataFrame
                to. If not provided, the default value from self.df_file_name 
                will be used.
        """
        
        # Using the default filename_unless otherwise stated
        if filename is None:
            filename = self.df_file_name
        
        path = os.path.join(self.data_path, filename+".csv")

        self.df.to_csv(os.path.join(path))


    def load_new_df(self, df_file_path:str, set_new_data_path: Optional[bool] = True, set_new_file_name: Optional[bool] = True) -> None:
        """
        Load a new DataFrame .csv file by supplying a path. With this method, a 
        DataFrame outside of the default data folder can be loaded.

        Parameter: 
            df_file_path (str): A path compatible string variabe pointing to the
                new DataFrame .csv file
            set_new_data_path (Optional[bool]): An optional parameter that 
                controls whether the directory path from the df_file_path string will
                be used to reset the self.data_path variable to the same directory
                path automatically. 
            set_new_file_name (Optional[bool]): An optional paramter that 
                controls whether the self.df_file_name paramter will be reset, 
                using the file name in the df_file_path specified.
        """
        if not isinstance(df_file_path, str):
            raise TypeError("The new DataFrame path specified has to be a valid path string.")
        
        dir_path, f_path = os.path.split(df_file_path)

        # print("load_new_df 1: dir_path",dir_path)
        # print("load_new_df 2: f_path",f_path)

        if set_new_data_path:
            self.set_data_path(dir_path, create_dir=False)
        
        if set_new_file_name: 
            file_name_without_extension, file_extension = os.path.splitext(f_path)
            self.df_file_name = file_name_without_extension
        
        self.df = pd.read_csv(os.path.join(df_file_path))

    def load_df(self, df_csv_file_name:str) -> None:
        """
        Loads the standard DataFrame file, found in the standard data path by 
        supplying a .csv file name without the .csv extension. You cannot load 
        a DataFrame file outside of the set data folder with this method.

        Parameter: 
            df_file_path (str): The .csv file name of the DataFrame
        """
        # print("load_df 0: self.data_path",self.data_path)
        if not isinstance(df_csv_file_name, str):
            raise TypeError("The .csv file name specified has to be a valid string.")


        # The assumption is that the data folder is always in the same directory as the boston_modeler.py file
        full_file_path = os.path.join(self.data_path, df_csv_file_name + ".csv") 
        file_name_with_extension = df_csv_file_name + ".csv" 
        # print("load_df 1: self.data_path",self.data_path)
        if self.save_check(file_name_with_extension):
            self.load_new_df(full_file_path)
            # print("load_df 2: self.data_path",self.data_path)
        else:
            warn(f"The file with path '{full_file_path}' could not be found. No DataFrame was loaded.")

    def save_model(self, filename:Optional[str] = None) -> None:
        """
        Save the trained model (probably trained) in self.model to a file.

        Parameters:
            filename (str, optional): The name of the file to save the Model to.
                If not provided, the default value from self.df_model_name will 
                be used.
        """
        
        # Using the default filename_unless otherwise stated
        if filename is None:
            filename = self.model_file_name
        
        path = os.path.join(self.data_path, filename+".pkl")
        with open(path,"wb") as file:
            pickle.dump(self.model,file)

    def load_new_model(self, model_file_path:str, set_new_data_path: Optional[bool] = True, set_new_file_name: Optional[bool] = True) -> None:

        """
        Load a new Model .pkl file by supplying a path. With this method, a 
        Model outside of the default data folder can be loaded.

        Parameter: 
            model_file_path (str): A path compatible string variabe pointing to 
                the new model .pkl file
            set_new_data_path (Optional[bool]): An optional parameter that 
                controls whether the directory path from the model_file_path string 
                will be used to reset the self.data_path variable to the same 
                directory path automatically.
            set_new_file_name (Optional[bool]): An optional paramter that 
                controls whether the self.model_file_name paramter will be reset, 
                using the file name in the model_file_path specified.
        """

        if not isinstance(model_file_path, str):
            raise TypeError("The new DataFrame path specified has to be a valid path string.")
        
        dir_path, f_path = os.path.split(model_file_path)

        if set_new_data_path:
            self.data_path = self.set_data_path(dir_path, create_dir=False)

        if set_new_file_name: 
            file_name_without_extension, file_extension = os.path.splitext(f_path)
            self.model_file_name = file_name_without_extension

        with open(model_file_path, "rb") as file:
            model = pickle.load(file)
        self.model = model



    def load_model(self, model_pkl_file_name:str, auto_train:Optional[bool]=False) -> None:
        """
        Load the standard Model file, found in the standard data path by 
        supplying a .pkl file name without the .pkl extension. You cannot load 
        a Model file outside of the set data folder with this method.

        Parameters:
            model_pkl_file_name (str): The name of the model file to load without the ext.
            auto_train(Optional[bool]): An option to automatically train if
                a DataFrame .csv file is available.
        """
        # The assumption is that the data folder is always in the same directory as the boston_modeler.py file
        # path = os.path.join(os.path.dirname(__file__),self.data_path, model_pkl_file_name + ".pkl")

        # self.load_new_model(path)

        if not isinstance(model_pkl_file_name, str):
            raise TypeError("The .pkl file name specified has to be a valid string.")

        # print("load_model 1: self.data_path", self.data_path)

        full_model_file_path = os.path.join(self.data_path, model_pkl_file_name + ".pkl")
        df_file_name_with_extension = model_pkl_file_name + ".csv" 
        
        full_df_file_path = os.path.join(self.data_path, self.df_file_name + ".csv") 
        df_file_name_with_extension = self.df_file_name + ".csv" 

        print("load_model - full_model_file_path", full_model_file_path)
        print("load_model - self.save_check(full_model_file_path)", self.save_check(full_model_file_path))
        # Checking if model file exists or whether a new model could be trained
        # print(f"######################################{full_model_file_path }}")
        if self.save_check(full_model_file_path):
            self.load_new_model(full_model_file_path)
        elif ((auto_train==True) and self.save_check(full_df_file_path)):
            msg = (
                f"The file with path '{full_model_file_path}' could not be found. "
                f"No Model was loaded. The DataFrame in '{full_df_file_path}' will"
                "now be used to train and save a model."
                )
            warn(msg)
            self.train()
            self.save_model()
            print(f"New model has beenn saved here: '{full_model_file_path}'")
        elif ((auto_train!=True) and self.save_check(full_df_file_path)):
            msg = (
                f"The file with path '{full_model_file_path}' could not be found." 
                "No Model was loaded. However, a DataFrame file was found in "
                f"'{full_df_file_path}'. A new model can be trained and saved if "
                "the 'auto_train' option is set to 'True'"
            )
            warn(msg)
        elif ((auto_train==True) and not self.save_check(full_df_file_path)):
            msg = (
                f"The file with path '{full_model_file_path}' could not be found." 
                "No Model was loaded. Training and saving of a new model cannot"
                f"happen as theres is no valid .csv file in '{full_df_file_path}'. "
                "Please supply a valid .csv DataFrame file first."
            )
            warn(msg)
        else:
            msg = (
                f"The file with path '{full_model_file_path}' could not be found." 
                "No Model was loaded."
            )
            warn(msg)


    def save_check(self, filename:str) -> bool:
        """
        Checks if a file with name specified has previously been saved in the
        data folder or not.

        Parameters:
            filename (str): The name of the file to load.

        Returns:
            bool: true if saved file exists and false if it does not exist.
        """
        path = os.path.join(self.data_path, filename)
        return os.path.exists(path)

    def return_df(self) -> pd.DataFrame:
        """
        Returns the dataframe used in the class instance.

        Returns:
            pd.DataFrame: the dataframe inside of the object instance.

        """
        return self.df

    def return_X_Y(self) -> Tuple[pd.DataFrame, np.ndarray]:
        """
        Returns the X training data used in the class instance and the original 
        Y target data used in the class instance.

        Returns:
            Tuple[pd.DataFrame, pd.DataFrame, np.ndarray]: X data set and the Y 
                dataset inside of the object instance.
        """
        return self.X, self.Y

    def return_model(self) -> Any:
        """
        Returns the model in the class instance.

        Returns:
            Any: Any model type in the class instance.
        """
        return self.model

    def return_file_names(self) -> Tuple[str, str]:
        """
        Returns the file names of the DataFrame and Model.

        Returns:
            Tuple[str, str]: A tuple containing the string file names of the 
                DataFrame and Model (DataFrame name, Model name). 
        """
        return (self.df_file_name, self.model_file_name)