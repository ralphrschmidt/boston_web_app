from typing import Tuple, Any, Optional
from abc import ABC, abstractmethod

import pandas as pd
import numpy as np


class Modeler(ABC):
    def __init__(self, model_type: str, data_path: Optional[str] = "../data"):
        """
        Initialize the Modeler instance. It is an Abstract Class.

        Parameters:
            model_type (str): The type of model to train.
            data_path (Optional[str]): Optional string to adjust the path of
                the data directory which may include the dataframe and saved models.
        """
        self.model_type = model_type
        self.data_path = data_path
    
    @abstractmethod
    def train(self) -> None:
        """
        Abstract method that looks at the model_type and chooses a model for 
        training (there should be some _trainModelType methods that can be 
        chosen)
        """
        pass

    @abstractmethod
    def predict(self, df:pd.DataFrame) -> np.ndarray:
        """
        An abstract method to make model predictions

        Parameters:
            df (pd.DataFrame): One observation of a model compatible dataframe.
        
        Returns:
            np.ndarray: An array containing the prediction.
        """
        return self.model.predict(df_slt)

    @abstractmethod
    def set_data_path(self, new_data_path:str, create_dir: Optional[bool] = True) -> None:
        """
        An abstract method to set the data directory path, that may include the 
        model picke file and the training .csv file.

        Parameter: 
            new_data_path (str): A path compatible string variabe
            create_dir (Optional[bool]): An optional parameter that controls 
                whether a new directory should be created if the given path
                does not exist. By default, a new directory will be made if it
                did not exist.
        """
        pass


    def save_df(self, filename:Optional[str] = None) -> None:
        """
        An abstract method to save the DataFrame to a file, into the default
        data folder.

        Parameters:
            filename (str, optional): The name of the file to save the DataFrame
                to. If not provided, the default value from self.df_file_name 
                will be used.
        """
        pass


    def load_new_df(self, df_file_path:str, set_new_data_path: Optional[bool] = True, set_new_file_name: Optional[bool] = True) -> None:
        """
        An abstract method to load a new DataFrame .csv file by supplying a
        path. With this method, a DataFrame outside of the default data folder 
        can be loaded.

        Parameter: 
            df_file_path (str): A path compatible string variabe pointing to the
                new DataFrame .csv file
            set_new_data_path (Optional[bool]): An optional parameter that 
                controls whether the directory path from the df_file_path string will
                be used to reset the self.data_path variable to the same directory
                path automatically. 
            set_new_file_name (Optional[bool]): An optional paramter that 
                controls whether the self.df_file_name paramter will be reset, 
                using the file name in the df_file_path specified.
        """
        pass

    def load_df(self, df_csv_file_name:str) -> None:
        """
        An abstract method to that loads the standard DataFrame file, found in 
        the standard data path by supplying a .csv file name without the 
        .csv extension. You cannot load a DataFrame file outside of the set 
        data folder with this method.

        Parameter: 
            df_file_path (str): The .csv file name of the DataFrame
        """
        pass

    def save_model(self, filename:Optional[str] = None) -> None:
        """
        An bastract method that saves the model (probably trained) in 
        self.model to a file.

        Parameters:
            filename (str, optional): The name of the file to save the Model to.
                If not provided, the default value from self.df_model_name will 
                be used.
        """
        pass

    def load_new_model(self, model_file_path:str, set_new_data_path: Optional[bool] = True, set_new_file_name: Optional[bool] = True) -> None:

        """
        Load a new model.pkl file by supplying a path.

        An abstract method to load a new Model .pkl file by supplying a
        path. With this method, a Model outside of the default data folder can 
        be loaded.


        Parameter: 
            model_file_path (str): A path compatible string variabe pointing to 
                the new model .pkl file
            set_new_data_path (Optional[bool]): An optional parameter that 
                controls whether the directory path from the model_file_path string 
                will be used to reset the self.data_path variable to the same 
                directory path automatically.
            set_new_file_name (Optional[bool]): An optional paramter that 
                controls whether the self.model_file_name paramter will be reset, 
                using the file name in the model_file_path specified.
        """
        pass

    def load_model(self, filename:str) -> None:
        """
        An abstract method to that loads the standard Model file, found in the 
        standard data path by supplying a .pkl file name without the .pkl 
        extension. You cannot load a Model file outside of the set data folder 
        with this method.

        Parameters:
            filename (str): The name of the model file to load without the ext.
        """
        pass

    @abstractmethod
    def save_check(self, filename:str) -> bool:
        """
        An abstract method to check if the model with name specified has 
        previously been saved.

        Parameters:
            filename (str): The name of the model file to load.

        Returns:
            bool: true if saved model exists and false if it does not exist.
        """
        pass

    @abstractmethod
    def return_df(self) -> pd.DataFrame:
        """
        Abstract method that returns the dataframe used in the class instance.

        Returns:
            pd.DataFrame: the dataframe inside of the object instance.

        """
        pass

    @abstractmethod
    def return_X_Y(self) -> Tuple[pd.DataFrame, np.ndarray]:
        """
        Abstract method that returns the X training data used in the class 
        instance and the original Y target data used in the class instance.

        Returns:
            Tuple[pd.DataFrame, pd.DataFrame, np.ndarray]: X data set and the Y 
                dataset inside of the object instance.
        """
        pass

    @abstractmethod
    def return_model(self) -> Any:
        """
        Abstract method that returns the model in the class instance.

        Returns:
            Any: Any model type in the class instance.
        """
        pass

    @abstractmethod
    def return_file_names(self) -> Tuple[str, str]:
        """
        Abstract method that returns the file names of the DataFrame and Model.

        Returns:
            Tuple[str, str]: A tuple containing the string file names of the 
                DataFrame and Model (DataFrame name, Model name). 
        """
        pass