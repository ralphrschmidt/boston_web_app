VENV = venv
PYTHON = $(VENV)/bin/python3
PIP = $(VENV)/bin/pip

.PHONY: install test run stop clean docker-build docker-run qtest

default: run

install: $(VENV)/bin/activate
	$(PIP) install -r requirements.txt

test: $(VENV)/bin/activate
	$(PYTHON) -m pytest -s tests/test_main.py

qtest: $(VENV)/bin/activate
	$(PYTHON) src/quick_tests.py



run: $(VENV)/bin/activate
	$(PYTHON) src/main.py start

stop: $(VENV)/bin/activate
	$(PYTHON) src/main.py stop

clean:
	$(PYTHON) src/main.py stop
	find . -type f -name '*.pyc' -delete
	find . -type d -name '__pycache__' -exec rm -rf {} +
	find . -type d -name 'venv' -exec rm -rf {} +

$(VENV)/bin/activate: requirements.txt
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt

docker-build:
	docker build -t myapp .

docker-run:
	docker run --rm -p 8000:8000 myapp
