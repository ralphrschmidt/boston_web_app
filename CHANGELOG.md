# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2024-05-04
### Added
- Initial setup of the web application.
- Basic layout and design of the web app established.
- Local deployment capabilities implemented, allowing for running the application on a local server.

### Notes
- This is the first rudimentary deployment of the web application, primarily focused on establishing a basic structure and deployment process.
- Many functionalities are still pending and will be developed in upcoming releases.
